import React, { Component } from "react";
import Button from "@material-ui/core/Button";

const WIDTH = 160;
const HEIGHT = 120;

const snapStyle = {
  height: HEIGHT,
  width: WIDTH
};

export default class SnapTaker extends Component {
  getSnapShot() {
    const d = function(tag) {
      const v = document.createElement(tag);

      v.width = WIDTH;
      v.height = HEIGHT;
      if (tag === "video") v.autoplay = true;
      return v;
    };
    const video = d("video");
    const canvas = d("canvas");

    window.cann = canvas;
    const context = canvas.getContext("2d");
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      // Not adding `{ audio: true }` since we only want video now
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then(function(stream) {
          //video.src = window.URL.createObjectURL(stream);
          video.srcObject = stream;
          video.play();
        })
        .then(() => {
          context.drawImage(video, 0, 0, WIDTH, HEIGHT);
          return canvas.toDataURL();
        })
        .then(v => {
          video.stop();
          return v;
        });
    }
  }

  startVideo(e) {
    e.preventDefault();
    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");
    var video = document.getElementById("video");
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      // Not adding `{ audio: true }` since we only want video now
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then(function(stream) {
          //video.src = window.URL.createObjectURL(stream);
          video.srcObject = stream;
          video.play();
        });
    }
  }

  takePicture(e) {
    e.preventDefault();
    return Promise.resolve()
      .then(() => {
        const canvas = document.getElementById("canvas");
        var context = canvas.getContext("2d");

        const video = document.getElementById("video");
        context.drawImage(video, 0, 0, WIDTH, HEIGHT);
        video.srcObject.getTracks()[0].stop();
        // return canvas.toDataURL();
        return new Promise(resolve => {
          canvas.toBlob(blob => resolve(blob));
        });
      })
      .then(pictureBlob => {
        return this.props.onPictureTaken(pictureBlob);
      });
  }

  render() {
    return (
      <div>
        <video id="video" width={WIDTH} height={HEIGHT} autoPlay />

        <canvas id="canvas" width={WIDTH} height={HEIGHT} />
        <Button onClick={e => this.startVideo(e)}>Start Video</Button>

        <Button onClick={e => this.takePicture(e)}>Take Picture</Button>
      </div>
    );
  }
}

