import React, {Component} from 'react';
import TranslateUtil from "../../utils/TranslateUtil";
import Select from '@material-ui/core/Select';
import FilledInput from "@material-ui/core/FilledInput/FilledInput";
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import FormLabel from "@material-ui/core/FormLabel/FormLabel";
import Button from "@material-ui/core/Button/Button";
import SettingsDB from "../../db/settings";
import db from "../../db/db";
import Popover from "@material-ui/core/Popover/Popover";


const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    select: {
        marginLeft: '10px',
        width: '100px'

    },
    duration: {
        marginLeft: '10px'
    },
    label: {
        paddingBottom: '10px',
    },
    langPicker: {
        paddingTop: '40px',
        width: '100%'

    }

});

class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            languages: [],
            language: '',
            targetLanguage: '',
            duration: '',
            deckSize: '',
            anchorEl: ''
        };

    }

    componentDidMount() {
        this.setLanguages();
        db.getOneById('settings').then(settings =>{
            this.setState({
               language: settings.language,
               targetLanguage: settings.targetLanguage,
               duration: settings.duration,
               deckSize: settings.deckSize,
            });
        });

    }

    setLanguages() {
        TranslateUtil.getLanguages().then(jsonData => {
            console.log(jsonData.data.languages);
            this.setState({languages: jsonData.data.languages});
        }).catch(error => console.log("err in set language:", error));
    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value});
    };

    handleSave = event => {
        const settings = new SettingsDB(this.state.language, this.state.targetLanguage, this.state.duration, this.state.deckSize);
        console.log(settings);
        db.edit(settings).then(() => {
                console.log('done');
                this.setState({ anchorEl: event.currentTarget,});
        }).catch(error => console.log("err in set settings to datbase:", error) );
    };

    handleClose = () => {
        this.setState({
            anchorEl: null,
        });
    };

    render() {
        const {classes} = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <div className={classes.container}>
                <div className={classes.langPicker}>
                    <FormLabel component="label" className={classes.label}>Your language</FormLabel>

                    <Select
                        native
                        value={this.state.language}
                        onChange={this.handleChange('language')}
                        input={<FilledInput name="language" id="filled-language-native-simple"
                                            className={classes.select}/>}
                    >

                        {this.state.languages.map(function (language) {
                            return <option value={language.language}>{language.language}</option>;
                        })}

                    </Select>

                </div>
                <div className={classes.langPicker}>
                    <FormLabel component="label" className={classes.label}>Language to learn</FormLabel>
                    <Select
                        native
                        value={this.state.targetLanguage}
                        onChange={this.handleChange('targetLanguage')}
                        input={<FilledInput name="targetLanguage" id="filled-target-language-native-simple"/>}
                        className={classes.select}
                    >

                        {this.state.languages.map(function (language) {
                            return <option value={language.language}>{language.language}</option>;
                        })}

                    </Select>
                </div>
                <div className={classes.langPicker}>
                    <FormLabel component="label" className={classes.label}>Lesson notification duration</FormLabel>
                    <Select
                        native
                        value={this.state.duration}
                        onChange={this.handleChange('duration')}
                        input={<FilledInput name="duration" id="filled-target-language-native-simple"/>}
                        className={classes.select}
                    >
                        <option value="0">1 hour</option>
                        <option value="1">4 hours</option>
                        <option value="2"> 1 day</option>
                        <option value="3">1 week</option>
                    </Select>
                </div>
                <div className={classes.langPicker}>
                    <FormLabel component="label" className={classes.label}>Card deck size</FormLabel>
                    <Select
                        native
                        value={this.state.deckSize}
                        onChange={this.handleChange('deckSize')}
                        input={<FilledInput name="deckSize" id="filled-target-language-native-simple"/>}
                        className={classes.select}
                    >

                        <option value="10">10 cards</option>
                        <option value="20">20 cards</option>
                        <option value="30">30 cards</option>
                        <option value="0">ALL</option>

                    </Select>
                </div>
                <div className={classes.langPicker}>
                    <Button  aria-owns={open ? 'simple-popper' : undefined} aria-haspopup="true" variant="contained" color="primary" size="large" onClick={this.handleSave}>
                        Save
                    </Button>
                    <Popover
                        id="simple-popper"
                        open={open}
                        onClose={this.handleClose}
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'center',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'center',
                        }}
                    >
                        Saved!
                    </Popover>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Settings);
Settings.propTypes = {
    classes: PropTypes.object.isRequired,
};

