import React, { Component } from "react";
/* eslint-disable*/
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import EditIcon from "@material-ui/icons/Edit";

import db from "../../db/db";
import FlashCard from "../../db/flashcard";

import "./index.css";
export default class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      words: []
    };
  }

  componentDidMount() {
    this.getWords();
  }

  getWords() {
    db.getAll().then(elements => {
      elements.rows.forEach(item => {
        this.setState({
          words: [
            ...this.state.words,
            { averse: item.doc.averse, reverse: item.doc.reverse }
          ]
        });
      });
      //this.setState({words:elements.rows.doc});
    });
  }

  renderList() {
    return this.state.words.map((item, index) => {
      return (
        <ListItem divider key={index}>
          <ListItemText primary={item.averse} />
          <ListItemText primary={item.reverse} />
          <ListItemIcon>
            {" "}
            <EditIcon />{" "}
          </ListItemIcon>
        </ListItem>
      );
    });
  }

  render() {
    return (
      <div className="Show">
        <List>
          {this.state.words.length !== 0 ? (
            this.renderList()
          ) : (
            <ListItem>
              <ListItemText>You don't have any words</ListItemText>
            </ListItem>
          )}
        </List>
      </div>
    );
  }
}

