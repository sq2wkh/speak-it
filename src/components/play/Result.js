import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import ResultCard from "./ResultCard";
import "./style.css";

export default class Result extends Component {
  render() {
    return (
      <div className="play_container">
        <div>
          <ResultCard
            header="Your result is"
            result={`${this.props.correct} of ${this.props.total}`}
          />

          <Button
            size="large"
            variant="contained"
            color="primary"
            onClick={() => this.props.repeater()}
          >
            Try new game
          </Button>
        </div>
      </div>
    );
  }
}

