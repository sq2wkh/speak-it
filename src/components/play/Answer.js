import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import ItemCard from "./ItemCard.js";

export default class Answer extends Component {
  render() {
    return (
      <div>
        <ItemCard
          word={this.props.word}
          header="The answer is"
          description={this.props.description}
          image={this.props.image}
        />
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={()=> this.props.finish(true)}
        >
          I guessed
        </Button>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          onClick={()=>this.props.finish(false)}
        >
          No
        </Button>
      </div>
    );
  }
}

