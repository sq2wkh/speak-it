import React from "react";

import LinearProgress from "@material-ui/core/LinearProgress";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  progress: {
    minHeight: 20,
    marginTop:20,
    height: 20
  }
};

function PlayProgress(props) {
  const { classes } = props;

  return (
    <LinearProgress
      className={classes.progress}
      color="primary"
      variant="determinate"
      value={props.value}
    />
  );
}

PlayProgress.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PlayProgress);

