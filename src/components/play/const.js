
export const PAGES = {
   QUESTION: "QUESTION",
   ANSWER: "ANSWER"
}


export const CARD_STYLES = {
  card: {
    minWidth: 275,
    minHeight: 300,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 12,
    marginBottom: 12
  },

  pos: {
    marginBottom: 12
  }
};
