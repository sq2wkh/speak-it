import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";

import CardContent from "@material-ui/core/CardContent";

import Typography from "@material-ui/core/Typography";
import { CARD_STYLES } from "./const";

function ResultCard(props) {
  const { classes } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {props.header}
        </Typography>
        <Typography variant="h4" component="h1" className="word">
          {props.result}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {props.description ? props.description : null}
        </Typography>
      </CardContent>
    </Card>
  );
}

ResultCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(CARD_STYLES)(ResultCard);

