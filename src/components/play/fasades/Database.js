import { dictionary } from "./translations";
import db from "../../../db/db";

export default class PlayDatabase {
  static dbSelect(count) {
    return db
      .getAll()
      .then(elements => {
        return elements.rows.map(item => ({
          origin: { word: item.doc.averse },
          translated: { word: item.doc.reverse }
        }));
      })
      .then(arr => [].concat(arr, dictionary))
      .then(arr => {


        if (!(arr.length > count)) return arr;
        const result = [];
        let buffer = arr.map(x => x); //copy
        while (result.length < count) {
          const element = buffer.splice(
            Math.floor(Math.random() * buffer.length),
            1
          );
          result.push(element[0]);
        }

        return result;
      });
  }
}

