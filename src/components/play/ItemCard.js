import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";

import CardContent from "@material-ui/core/CardContent";

import Typography from "@material-ui/core/Typography";
import {CARD_STYLES} from "./const";



function ItemCard(props) {
  const { classes } = props;


  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {props.header}
        </Typography>
        <Typography variant="h4" component="h1" className="word">
          {props.word}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {props.description ? props.description : null}
        </Typography>
        {props.image ? (
          <Typography component="p">
            <img src={props.image.url} alt="NoImage" />
          </Typography>
        ) : null}
      </CardContent>
    </Card>
  );
}

ItemCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(CARD_STYLES)(ItemCard);

