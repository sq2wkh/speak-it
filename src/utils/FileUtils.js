export default class FileUtils {
  static getBase64String(file) {
    return new Promise(resolve => {
      const reader = new FileReader();

      reader.onload = function(e) {
        resolve(reader.result);
      };

      reader.readAsDataURL(file);
    }).then(base64String => {
      return base64String;
    });
  }
}

