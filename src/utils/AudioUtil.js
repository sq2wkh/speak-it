const apiKey = 'AIzaSyDrq392ITzbeg7SDIHcQ2d4jVLgoElYtsI';

class AudioUtil {


    static getRecognition(aaudioBucketUri) {
        let audio = {"uri" : aaudioBucketUri};
        let config ={
            "languageCode": "en-US",
            "encoding": "FLAC",
            "sampleRateHertz": 16000
        };
        let requests = { audio , config};
        console.log(JSON.stringify(requests, null, 2));
        return fetch('https://speech.googleapis.com/v1/speech:recognize?key=' + apiKey,{
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              //   Authorization: "Bearer " + apiKey,
            },
            body: JSON.stringify(requests)

        }).then(
            function (response) {
                return response.json();
            }
        )
    }

}

export default AudioUtil;
