const apiKey = 'AIzaSyDrq392ITzbeg7SDIHcQ2d4jVLgoElYtsI';

class TranslateUtil {


    static getLanguages() {
        return fetch('https://translation.googleapis.com/language/translate/v2/languages?key=' + apiKey).then(
            function (response) {
                return response.json();
            }
        )
    }

    static translate(sourceLang, targetLang, word) {

        return fetch('https://translation.googleapis.com/language/translate/v2?'
            + 'key=' + apiKey
            + '&source=' + sourceLang
            + '&target=' + targetLang
            + '&q=' + word
            , {method: 'POST'}).then(
            function (response) {
                return response.json();
            }
        )

    }


}

export default TranslateUtil;



