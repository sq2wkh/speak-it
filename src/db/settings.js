class SettingsDB {


    constructor (language, targetLanguage, duration, deckSize) {
        this.language = language;
         this.targetLanguage = targetLanguage;
         this.duration = duration;
         this.deckSize = deckSize;
         this._id = 'settings';

    }


}
export default SettingsDB;