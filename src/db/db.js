import PouchDB from 'pouchdb';
const db = new PouchDB('flashcards');
export default {

  /**
   * add - Add one item to Database
   *
   * @param  {object} document Some document to add.
   *    _id = 0 is for config document
   *    numeric id <1,infinity> for flashcards
   *    For any other kind of documents it is custom id (it can be string or number)
   * @return {Promise} Promise to do when request is done
   */
  add(document) {
      return db.put(document);
  },
  /**
   * getAll - Get All documents
   *
   * @return {Promise}  Promise to do when request is done
   */
  getAll() {
    return db.allDocs({
  include_docs: true,
  attachments: true
});
  },
  /**
   * getOneById - fetch one document by its ID
   *
   * @param  {string} id id of document
   * @return {Promise}    description
   */
  getOneById(id) {
    return db.get(id);
  },
  /**
   * edit - Edit document
   *
   * @param  {Object} item new version of document
   * @return {Promise}      description
   */

  edit(item) {
    return db.get(item._id).then((doc) => {
      item._rev = doc._rev
        return db.put(item);
    });
  },

  /**
   * delete - Delete document from database
   *
   * @param  {string} id ID of document
   * @return {Promise}    description
   */
  delete(id) {
    return db.remove(id);
  }

}
