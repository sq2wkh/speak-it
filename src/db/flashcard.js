class FlashCard {

  /**
   * constructor - description
   *
   * @param  {string} averse  averse of flashcard
   * @param  {string} reverse averse of flashcard
   */
  constructor (averse, reverse) {
    this.averse = averse;
    this.reverse = reverse;
    this._id = ''+this.generateId();
  }

  generateId() {
    let first, second;
    first = (this.averse.length < 3) ? this.averse : this.averse.substr(0,3);
    second = (this.reverse.length < 3) ? this.reverse : this.reverse.substr(0,3)
    return first+second;
  }
}
export default FlashCard;
